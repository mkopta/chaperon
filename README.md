# chaperon

Quick and dirty way for a developer to get his repositories cloned and updated.

## Actions

* `chaperon clone` to clone all repositories. It will only clone those that you
  don't already have. List of repositories to clone is contained in
  `chaperon.list`.
* `chaperon notmaster` lists all git repositories in current directory that
  aren't on master branch.
* `chaperon notclean` lists all git repositories in current directory that have
  some uncommited changes.
* `chaperon update` goes through all git repositories in current directory and
  tries to update them (switch to master, pull changes, drop missing remote
  branches and update local master).
* `chaperon coderage` finds random source code file and loads it with vim
* `chaperon notmerged` goes through all git repositories in current directory
  and checks whether there are some branches not merged to master.
* `chaperon merged` goes through all git repositories in current directory and
  checks whether there are some branches already merged to master.
  (candidates for removal)
* `chaperon make TARGET` goes through all directories in current directory and
  calls `make TARGET` if it is supported. Stops at first error and cwd is set
  to repo where error happened.


## Usage

```
cd dev
git clone git@github.com:mkopta/chaperon.git
cat > chaperon.list <<EOF
git@github.com:mkopta/chaperon.git
git@github.com:my/repo.git
git@github.com:my/another_repo.git
EOF
chaperon/chaperon clone
chaperon/chaperon update
```
